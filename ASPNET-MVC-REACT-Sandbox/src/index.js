import "./index.less";
import "bootstrap";
import * as React from "react";
import * as ReactDOM from "react-dom";
import Hello from "./components/Hello";
ReactDOM.render(React.createElement(Hello, { compiler: "Typescript" }), document.getElementById("example"));
//# sourceMappingURL=index.js.map